<!DOCTYPE html>
<html>

<head>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Workgress</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist2/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="dist2/css/landing-page1.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/dropdown.css'); ?>" type="text/css" media="screen">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>


   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
  <script src="plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="plugins/toastr/toastr.min.js"></script>
  
  <!-- Animate.css -->
	<link rel="stylesheet" href="assets/course/css/animate.css">

  <!-- Theme style  -->
  <link rel="stylesheet" href="assets/course/css/style.css">

  <!-- Modernizr JS -->
  <script src="assets/course/js/modernizr-2.6.2.min.js"></script>

  </head>

  <?php
	$this->session = \Config\Services::session();
	if($this->session->get("Role_name") == 'student'){
		$role = 'นักเรียน';
	 }else if($this->session->get("Role_name") == 'teacher'){
		$role = 'คุณครู';
	 }else if($this->session->get("Role_name") == 'admin'){
		$role = 'ผู้ดูแล';
	 }
?>
<?php
	 	if (session('correct')) : ?>
<script type="text/javascript">
    $(function () {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 5000
      });
        Toast.fire({
          icon: 'success',
          title: '<?php echo session('correct')?>'
        })
      });
      </script>
<?php
	 	elseif (session('incorrect')) : ?>
     <script type="text/javascript">
    $(function () {
      $(document).Toasts('create', {
          class: 'bg-danger',
          title: 'Workgress',
          subtitle: 'กรุณาลองใหม่',
          body: '<?php echo session('incorrect')?>'
        })
      });
      </script>
      <?php
	 	elseif (session('warning')) : ?>
     <script type="text/javascript">
    $(function () {
      $(document).Toasts('create', {
          class: 'bg-warning',
          title: 'Workgress',
          subtitle: 'กรุณาลองใหม่',
          body: '<?php echo session('warning')?>'
        })
      });
      </script>
      <?php
	 	endif 
	 ?>
<body class="hold-transition layout-top-nav">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
        <a href="<?php echo base_url('/home');?>" class="navbar-brand">

          <span class="brand-text font-weight-light">WG</span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
          aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
          <!-- Left navbar links -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a href="<?php echo base_url('/home');?>" class="nav-link">Home</a>
            </li>
            <li class="nav-item dropdown">
              <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                class="nav-link dropdown-toggle">Category <i class="fas fa-th-large"></i></a>
              <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="#" class="dropdown-item">Some action </a></li>
                <li><a href="#" class="dropdown-item">Some other action</a></li>

                <li class="dropdown-divider"></li>

                <!-- Level two dropdown-->
                <li class="dropdown-submenu dropdown-hover">
                  <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false" class="dropdown-item dropdown-toggle">Hover for action</a>
                  <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li>
                      <a tabindex="-1" href="#" class="dropdown-item">level 2</a>
                    </li>

                    <!-- Level three dropdown-->
                    <li class="dropdown-submenu">
                      <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" class="dropdown-item dropdown-toggle">level 2</a>
                      <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                        <li><a href="#" class="dropdown-item">3rd level</a></li>
                        <li><a href="#" class="dropdown-item">3rd level</a></li>
                      </ul>
                    </li>
                    <!-- End Level three -->

                    <li><a href="#" class="dropdown-item">level 2</a></li>
                    <li><a href="#" class="dropdown-item">level 2</a></li>
                  </ul>
                </li>
                <!-- End Level two -->
              </ul>
            </li>
          </ul>
        
          <!-- SEARCH FORM -->
          <ul class="nav navbar-nav mx-auto">
          <form class="form-inline ml-0 ml-md-3">
            <div class="input-group">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                </div>
              </div>
            </form>
            </ul>

      
      



        <!-- Right navbar links -->
        <ul class="nav navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->

          <!-- Notifications Dropdown Menu -->

          <div class="nav-item">

            <label for="profile2" class="profile-dropdown">
              <input type="checkbox" id="profile2">
              <?php
	  if($this->session->get("Picture")){?>
              <img src="<?php echo $this->session->get("Picture");?>"><?php
	  }
	  else{?>
              <img src="<?php echo base_url('assets/img/profile.jpg');?>"><?php
	  }
	  ?>
              <span><?php 
					echo $role.' '.$this->session->get("Full_name"); 	
					?></span>
              <label for="profile2"><i class="mdi mdi-menu"></i></label>
              <ul>
                <li><a href="<?php echo base_url('/profile');?>"><i class="mdi mdi-account"></i>Profile</a></li>
                <?php
			if($this->session->get("Role_name") == 'student' ){
			?>
                <li><a href="<?php echo base_url('/teacher');?>"><i
                      class="mdi mdi-settings"></i>สอนบน Workgress</a></li>
                <?php
			}else if($this->session->get("Role_name") == 'admin'){
			?>
                <!-- <li><a href="<?php echo base_url('/showuser');?>"><i class="mdi mdi-logout"></i>จัดการ USER</a></li> -->
                <li><a href="<?php echo base_url('/dashboard');?>"><i class="mdi mdi-logout"></i>Admin</a></li>
                <li><a href="<?php echo base_url('/addcourse');?>"><i class="mdi mdi-logout"></i>เพิ่ม Course</a></li>
                <?php
			}else if($this->session->get("Role_name") == 'teacher' ){
			?>
                <li><a href="<?php echo base_url('/addcourse');?>"><i class="mdi mdi-logout"></i>เพิ่ม Course</a></li>
                <?php
			}
			?>
                <li><a href="#"><i class="mdi mdi-logout"></i>Course</a></li>
                <li><a href="<?= site_url('/UserController/User_Logout')?>"><i class="mdi mdi-logout"></i>Logout</a>
                </li>
              </ul>
            </label>
          </div>
        </ul>
      </div>
    </nav>
    <!-- /.navbar -->

    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <header class="masthead text-white text-center">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-xl-9 mx-auto">
              <h1 class="mb-5">ยินดีต้อนรับเข้าสู่ Workgress</h1>
              <h3>คุณพร้อมที่จะเรียนรู้สิ่งใหม่หรือยัง
              </h3>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
              <form>
                <div class="form-row">


                </div>
              </form>
            </div>
          </div>
        </div>
      </header>


      <!-- /.content-header -->

      <!-- Main content -->

      <div class="colorlib-loader"></div>
        <div class="colorlib-classes">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-md-offset-2 text-center colorlib-heading animate-box">
                <h2>Our Classes</h2>
                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost
                  unorthographic life One day however a small line of blind text by the name</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 animate-box">
                <div class="classes">
                  <div class="classes-img" style="background-image: url(<?php echo base_url('assets/course/images/classes-1.jpg');?>);">
                    <span class="price text-center"><small>$450</small></span>
                  </div>
                  <div class="desc">
                    <h3><a href="#">Developing Mobile Apps</a></h3>
                    <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                    <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 animate-box">
                <div class="classes">
                  <div class="classes-img" style="background-image: url(<?php echo base_url('assets/course/images/classes-2.jpg');?>);">
                    <span class="price text-center"><small>$450</small></span>
                  </div>
                  <div class="desc">
                    <h3><a href="#">Convert PSD to HTML</a></h3>
                    <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                    <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 animate-box">
                <div class="classes">
                  <div class="classes-img" style="background-image: url(<?php echo base_url('assets/course/images/classes-3.jpg');?>);">
                    <span class="price text-center"><small>$450</small></span>
                  </div>
                  <div class="desc">
                    <h3><a href="#">Convert HTML to WordPress</a></h3>
                    <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                    <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 animate-box">
                <div class="classes">
                  <div class="classes-img" style="background-image: url(<?php echo base_url('assets/course/images/classes-4.jpg');?>);">
                    <span class="price text-center"><small>$450</small></span>
                  </div>
                  <div class="desc">
                    <h3><a href="#">Developing Mobile Apps</a></h3>
                    <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                    <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 animate-box">
                <div class="classes">
                  <div class="classes-img" style="background-image: url(<?php echo base_url('assets/course/images/classes-5.jpg');?>);">
                    <span class="price text-center"><small>$450</small></span>
                  </div>
                  <div class="desc">
                    <h3><a href="#">Learned Smoke Effects</a></h3>
                    <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                    <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 animate-box">
                <div class="classes">
                  <div class="classes-img" style="background-image: url(<?php echo base_url('assets/course/images/classes-6.jpg');?>);">
                    <span class="price text-center"><small>$450</small></span>
                  </div>
                  <div class="desc">
                    <h3><a href="#">Convert HTML to WordPress</a></h3>
                    <p>Pointing has no control about the blind texts it is an almost unorthographic life</p>
                    <p><a href="#" class="btn-learn">Learn More <i class="icon-arrow-right3"></i></a></p>
                  </div>
                </div>
              </div>
            </div>
          
        </div>

        <div id="colorlib-subscribe" class="subs-img" style="background-image: url(<?php echo base_url('assets/course/images/img_bg_2.jpg');?>);"
          data-stellar-background-ratio="0.5">
          <div class="overlay"></div>
          <div class="container">


          </div>
        </div>
      </div>


      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Profile</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <strong>Copyright &copy; 2020 <a href="$">Workgress</a>.</strong>
      All rights reserved.
      <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1
      </div>
    </footer>
  </div>
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">เข้าสู่บัญชี Workgress ของคุณ</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="card">
            <div class="card-body login-card-body">
              <p class="login-box-msg">เข้าสู่ระบบเพื่อเริ่มระบบของคุณ</p>

              <form action="../../index3.html" method="post">
                <div class="input-group mb-3">
                  <input type="email" class="form-control" placeholder="Email">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-envelope"></span>
                    </div>
                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="password" class="form-control" placeholder="Password">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-lock"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-8">
                    <div class="icheck-primary">
                      <input type="checkbox" id="remember">
                      <label for="remember">
                        Remember Me
                      </label>
                    </div>
                  </div>
                  <!-- /.col -->
                  <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>


              <p class="mb-1">
                <a href="forgot-password.html">I forgot my password</a>
              </p>
              <p class="mb-0">
                <a href="register.html" class="text-center">Register a new membership</a>
              </p>
            </div>
            <!-- /.login-card-body -->
          </div>

        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <!-- Content Wrapper. Contains page content -->
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist2/js/adminlte.min.js"></script>

  <!-- Waypoints -->
  <script src="assets/course/js/jquery.waypoints.min.js"></script>

  <!-- Flexslider -->
  <script src="assets/course/js/jquery.flexslider-min.js"></script>

  <!-- Main -->
  <script src="assets/course/js/main.js"></script>
</body>

</html>

</html>