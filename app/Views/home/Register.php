<?php
include('config_google.php');
include('config_facebook.php');
//session_destroy();

//google_btn
 $login_button = $google_client->createAuthUrl();

 //facebook_btn
 $facebook_helper = $facebook->getRedirectLoginHelper();

 $facebook_permissions = ['email']; // Optional permissions
 $facebook_login_url = $facebook_helper->getLoginUrl('https://wg-test.herokuapp.com/UserFacebookController/Facebook_Login', $facebook_permissions);
 //$facebook_login_url = '<a href="'.$facebook_login_url.'"><img src="assets\img\btn_facebook.png" width="300" height="60"/></a>';

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Top Navigation</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist2/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="dist2/css/landing-page1.css" rel="stylesheet">
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="index3.html" class="navbar-brand">
       
        <span class="brand-text font-weight-light">WG</span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="index3.html" class="nav-link">Home</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">Contact</a>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Category <i
              class="fas fa-th-large"></i></a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="#" class="dropdown-item">Some action </a></li>
              <li><a href="#" class="dropdown-item">Some other action</a></li>

              <li class="dropdown-divider"></li>

              <!-- Level two dropdown-->
              <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Hover for action</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                  <li>
                    <a tabindex="-1" href="#" class="dropdown-item">level 2</a>
                  </li>

                  <!-- Level three dropdown-->
                  <li class="dropdown-submenu">
                    <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">level 2</a>
                    <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                      <li><a href="#" class="dropdown-item">3rd level</a></li>
                      <li><a href="#" class="dropdown-item">3rd level</a></li>
                    </ul>
                  </li>
                  <!-- End Level three -->

                  <li><a href="#" class="dropdown-item">level 2</a></li>
                  <li><a href="#" class="dropdown-item">level 2</a></li>
                </ul>
              </li>
              <!-- End Level two -->
            </ul>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-0 ml-md-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Messages Dropdown Menu -->
      
        <!-- Notifications Dropdown Menu -->
		<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-default">
                  <b>Sign In</b>
		</button>
		<div class="magin-ll">
    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-default">
                  <b>Sign Up</b>
		</button>
    </div>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<header class="masthead">
    <div class="overlay"></div>
    <div class="container">
    
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Register <small>jQuery Validation</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword2">Password</label>
                    <input type="password" name="password_confirm" class="form-control" id="exampleInputPassword2" placeholder="Password">
                  </div>
                  
                  <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>  
              </form>
            </div>
    <!-- /.form-box -->
 
    </div>
  </header>


    <!-- /.content-header -->

    <!-- Main content -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Profile</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">เข้าสู่บัญชี Workgress ของคุณ</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
				<div class="card">
					<div class="card-body login-card-body">
					<p class="login-box-msg">เข้าสู่ระบบเพื่อเริ่มระบบของคุณ</p>

					<form action="<?= site_url('/UserController/User_Login')?>" method="post" role="form" id="quickForm">
						<div class="input-group mb-3">
					
                   		 <input type="email" name="Email_Login"  id="Email_Login" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						<div class="input-group-append">
							<div class="input-group-text">
							<span class="fas fa-envelope"></span>
							</div>
						</div>
						</div>				
						<span id="email_check_login"></span>
						<div class="input-group mb-3">
					
                    	<input type="password" name="Password_Login" class="form-control" id="exampleInputPassword1" placeholder="Password">
						<div class="input-group-append">
							<div class="input-group-text">
							<span class="fas fa-lock"></span>
							</div>
						</div>
						</div>
						<div class="row">
						<div class="col-8">
						</div>
						<!-- /.col -->
						<div class="col-4">
							<button type="submit" class="btn btn-primary btn-block">Sign In</button>
						</div>
						<!-- /.col -->
						</div>
					</form>

					<div class="social-auth-links text-center mb-3">
						<p>- OR -</p>
						<a href="<?php echo $facebook_login_url?>" class="btn btn-block btn-primary">
						<i class="fab fa-facebook mr-2"></i> Sign in using Facebook
						</a>
						<a href="<?php echo $login_button?>" class="btn btn-block btn-danger">
						<i class="fab fa-google-plus mr-2"></i> Sign in using Google+
						</a>
					</div>
					<!-- /.social-auth-links -->

					<p class="mb-1">
						<a href="forgot-password.html">I forgot my password</a>
					</p>
					<p class="mb-0">
						<a href="<?php echo base_url('/register');?>" class="text-center">Register a new membership</a>
					</p>
					</div>
					<!-- /.login-card-body -->
				</div>

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

  <!-- Content Wrapper. Contains page content -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<!-- AdminLTE App -->
<script src="dist2/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist2/js/demo.js"></script>
</body>
</html>

<!-- Check ของ Login -->
<script type="text/javascript">
 $(document).ready(function(){  
      $('#Email_Login').change(function(){  
		   var Email_Login = $('#Email_Login').val(); 
           //var Email_Login = document.getElementById("Email_Login").value; 
		   console.log(Email_Login);
           if(Email_Login != '')  
           {  
                $.ajax({  
                     url:"<?= site_url('/UserController/Check_Email_Login')?>",  
                     method:"POST",  
                     data:{Email_Login:Email_Login},  
                     success:function(data){  
                          $('#email_check_login').html(data);  
                     }  
                });  
           } 
      });  
 });
 $(".toggle-password").click(function() {
$(this).toggleClass("fa-eye fa-eye-slash");
var input = $($(this).attr("toggle"));
if (input.attr("type") == "password") {
  input.attr("type", "text");
} else {
  input.attr("type", "password");
}
});   
 </script>
 <script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
 var value = $("#exampleInputPassword1").val();
$.validator.addMethod("checklower", function(value) {
  return /[a-z]/.test(value);
});
$.validator.addMethod("checkupper", function(value) {
  return /[A-Z]/.test(value);
});
$.validator.addMethod("checkdigit", function(value) {
  return /[0-9]/.test(value);
});
$.validator.addMethod("pwcheck", function(value) {
  return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value) && /[A-Z]/.test(value);
});
$.validator.addMethod("pwcheck", function(value) {

return /^[a-zA-Z0-9!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]+$/.test(value)
 && /[a-z]/.test(value) // has a lowercase letter
 && /\d/.test(value)//has a digit
 && /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value)// has a special character
});

  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
          minlength: 6,
        maxlength: 30,
        required: true,
        pwcheck: true,
        checklower: true,
        checkupper: true,
        checkdigit: true
      },
      terms: {
        required: true
      },
      password_confirm : {
                    minlength : 5,
                    equalTo : "#password"
                },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
      pwcheck: "must consist  lowercase letter, number and special characters",
      checklower: "Need atleast 1 lowercase alphabet",
      checkupper: "Need atleast 1 uppercase alphabet",
      checkdigit: "Need atleast 1 digit"
      },
      terms: "Please accept our terms",
      password_confirm:"NOT SAME"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
 var value = $("#exampleInputPassword1").val();
$.validator.addMethod("checklower", function(value) {
  return /[a-z]/.test(value);
});
$.validator.addMethod("checkupper", function(value) {
  return /[A-Z]/.test(value);
});
$.validator.addMethod("checkdigit", function(value) {
  return /[0-9]/.test(value);
});
$.validator.addMethod("pwcheck", function(value) {
  return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value) && /[A-Z]/.test(value);
});
$.validator.addMethod("pwcheck", function(value) {

return /^[a-zA-Z0-9!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]+$/.test(value)
 && /[a-z]/.test(value) // has a lowercase letter
 && /\d/.test(value)//has a digit
 && /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value)// has a special character
});

  $('#registerform').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
          minlength: 6,
        maxlength: 30,
        required: true,
        pwcheck: true,
        checklower: true,
        checkupper: true,
        checkdigit: true
      },
      terms: {
        required: true
      },
      password_confirm : {
                    minlength : 5,
                    equalTo : "#password"
                },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
      pwcheck: "must consist  lowercase letter, number and special characters",
      checklower: "Need atleast 1 lowercase alphabet",
      checkupper: "Need atleast 1 uppercase alphabet",
      checkdigit: "Need atleast 1 digit"
      },
      terms: "Please accept our terms",
      password_confirm:"NOT SAME"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
 <?php include 'Register_Validate_Script.php';?>
</body>
</html>
